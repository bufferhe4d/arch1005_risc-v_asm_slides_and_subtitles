1
00:00:00,040 --> 00:00:04,120
so the takeaways from this code we once

2
00:00:01,800 --> 00:00:05,600
again played the game of hiding the

3
00:00:04,120 --> 00:00:07,839
value that it's going to be shifted by

4
00:00:05,600 --> 00:00:10,240
so that the compiler couldn't generate a

5
00:00:07,839 --> 00:00:11,920
shift immediate and we played the game

6
00:00:10,240 --> 00:00:14,679
of changing up the size so that we're

7
00:00:11,920 --> 00:00:18,160
using a 32-bit int and when we're using

8
00:00:14,679 --> 00:00:20,600
32-bit values with 64-bit RV64I

9
00:00:18,160 --> 00:00:26,320
registers we're going to be using those

10
00:00:20,600 --> 00:00:28,119
W variant instructions silw surl W and S

11
00:00:26,320 --> 00:00:30,800
let's go ahead and collect those

12
00:00:28,119 --> 00:00:33,840
dutifully as we do over here only in the

13
00:00:30,800 --> 00:00:35,800
64-bit column once again W suffix things

14
00:00:33,840 --> 00:00:37,559
and oh hey looks like we've got some

15
00:00:35,800 --> 00:00:39,280
more W suffix things and then we're

16
00:00:37,559 --> 00:00:42,000
going to be done with this oh not quite

17
00:00:39,280 --> 00:00:43,960
one more up there anyways on our diagram

18
00:00:42,000 --> 00:00:46,440
popping those up they seem to be

19
00:00:43,960 --> 00:00:48,680
suspiciously displaced from their

20
00:00:46,440 --> 00:00:50,399
neighbors over here so perhaps we'll

21
00:00:48,680 --> 00:00:53,000
fill in those last three in the next

22
00:00:50,399 --> 00:00:53,000
section

