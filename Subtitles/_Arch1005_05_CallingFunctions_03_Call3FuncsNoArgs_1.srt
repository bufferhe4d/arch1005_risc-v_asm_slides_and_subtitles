1
00:00:00,240 --> 00:00:04,400
so I told you there's another inference

2
00:00:02,200 --> 00:00:06,240
game to be played and now it's time to

3
00:00:04,400 --> 00:00:08,360
play it we said that when we were

4
00:00:06,240 --> 00:00:10,240
calling one function with no args that

5
00:00:08,360 --> 00:00:12,040
we'd like to see how the stack behaves

6
00:00:10,240 --> 00:00:13,880
if we called more functions we wanted to

7
00:00:12,040 --> 00:00:16,560
see if there was a clear distinction

8
00:00:13,880 --> 00:00:17,880
between Leaf nodes the last function

9
00:00:16,560 --> 00:00:20,080
that's called that calls no more

10
00:00:17,880 --> 00:00:22,880
functions and the other functions

11
00:00:20,080 --> 00:00:24,960
interior nodes that call other functions

12
00:00:22,880 --> 00:00:27,199
so we now are calling three functions

13
00:00:24,960 --> 00:00:29,400
with no args we are compiling with o0

14
00:00:27,199 --> 00:00:31,279
again and we get this assembly which

15
00:00:29,400 --> 00:00:33,440
I've broken up for your viewing

16
00:00:31,279 --> 00:00:35,160
convenience here's what the stack looks

17
00:00:33,440 --> 00:00:36,960
like to start with and you know that

18
00:00:35,160 --> 00:00:39,360
this is the hint that it's going to be

19
00:00:36,960 --> 00:00:41,680
time to step through the assembly and

20
00:00:39,360 --> 00:00:43,399
draw a stack diagram now you're going to

21
00:00:41,680 --> 00:00:46,120
focus on and I would recommend in your

22
00:00:43,399 --> 00:00:50,280
drawing try to keep track of which frame

23
00:00:46,120 --> 00:00:50,280
is associated with which function

