1
00:00:00,080 --> 00:00:04,680
now we have another common example of

2
00:00:01,800 --> 00:00:07,240
control flow a while loop so we take an

3
00:00:04,680 --> 00:00:11,480
input from the command lines put it into

4
00:00:07,240 --> 00:00:13,880
a while a is greater than zero a minus

5
00:00:11,480 --> 00:00:16,320
minus print high and return sold face

6
00:00:13,880 --> 00:00:18,520
when you're done so emphasis on the a

7
00:00:16,320 --> 00:00:20,800
greater than Z check because when we

8
00:00:18,520 --> 00:00:22,920
look at the assembly we have a branch if

9
00:00:20,800 --> 00:00:24,480
greater than zero but I'm telling you

10
00:00:22,920 --> 00:00:26,400
right up front that's a pseudo

11
00:00:24,480 --> 00:00:29,199
instruction they're not what they appear

12
00:00:26,400 --> 00:00:31,880
to be because a branch if greater than

13
00:00:29,199 --> 00:00:34,920
zero is actually using a branch less

14
00:00:31,880 --> 00:00:37,960
than and it's just hardcoding the first

15
00:00:34,920 --> 00:00:39,879
argument to the zero register so you

16
00:00:37,960 --> 00:00:43,079
have your input branch of greater than

17
00:00:39,879 --> 00:00:45,280
zero you know a if the register was set

18
00:00:43,079 --> 00:00:47,559
to the value the current value of a then

19
00:00:45,280 --> 00:00:51,600
that would be placed here and so it's

20
00:00:47,559 --> 00:00:53,640
checking is zero less than your value

21
00:00:51,600 --> 00:00:56,000
and if so well then that means your

22
00:00:53,640 --> 00:00:58,199
value is greater than zero and I'll just

23
00:00:56,000 --> 00:01:01,559
point out this is the signed branch of

24
00:00:58,199 --> 00:01:04,799
Le then not the BLT U the

25
00:01:01,559 --> 00:01:07,960
BLT then we have a variant of that quick

26
00:01:04,799 --> 00:01:10,119
change while example two is now using a

27
00:01:07,960 --> 00:01:13,240
less than zero check everything else the

28
00:01:10,119 --> 00:01:15,920
same and indeed we get a new pseudo

29
00:01:13,240 --> 00:01:18,840
assembly instruction of branch if less

30
00:01:15,920 --> 00:01:21,720
than zero unmask the visitor to reveal

31
00:01:18,840 --> 00:01:24,400
his lizard face it's even got a nice

32
00:01:21,720 --> 00:01:27,560
little lizard tongue looks so fun but

33
00:01:24,400 --> 00:01:31,320
branch if less than zero is again a BLT

34
00:01:27,560 --> 00:01:34,680
a branch if less than but here the

35
00:01:31,320 --> 00:01:37,720
source to is hardcoded to zero so is

36
00:01:34,680 --> 00:01:40,720
your value less than zero makes sense

37
00:01:37,720 --> 00:01:43,399
right so once again we have ins cidio

38
00:01:40,720 --> 00:01:46,880
pseudo instructions impinging upon the

39
00:01:43,399 --> 00:01:48,600
Elegance of our simple branch block but

40
00:01:46,880 --> 00:01:50,320
here we go two new pseudo assembly

41
00:01:48,600 --> 00:01:52,119
instructions branch greater than zero

42
00:01:50,320 --> 00:01:55,719
branch less than zero but they're

43
00:01:52,119 --> 00:01:57,719
actually just both branch less than okay

44
00:01:55,719 --> 00:01:59,680
so I want you to go ahead and step

45
00:01:57,719 --> 00:02:01,320
through the assembly not so much because

46
00:01:59,680 --> 00:02:03,399
it's it's hard to understand these new

47
00:02:01,320 --> 00:02:05,560
branch types but rather because you

48
00:02:03,399 --> 00:02:08,000
should focus on understanding what a

49
00:02:05,560 --> 00:02:09,319
while loop looks like in assembly if you

50
00:02:08,000 --> 00:02:11,239
were going to be doing something like

51
00:02:09,319 --> 00:02:16,000
reverse engineering you would want to

52
00:02:11,239 --> 00:02:16,000
know when you're looking at a while loop

