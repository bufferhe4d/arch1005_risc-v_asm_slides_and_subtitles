1
00:00:00,120 --> 00:00:04,480
so what you should have been able to

2
00:00:01,480 --> 00:00:07,000
infer from our existing things is that

3
00:00:04,480 --> 00:00:09,440
all A's is going to push down these

4
00:00:07,000 --> 00:00:12,559
other local variables on our diagram and

5
00:00:09,440 --> 00:00:15,759
at 90oc remember this is the upper 32

6
00:00:12,559 --> 00:00:18,320
bits 90c is where the A's are going to

7
00:00:15,759 --> 00:00:20,439
go and that really is the power of

8
00:00:18,320 --> 00:00:22,199
inference just watching and playing with

9
00:00:20,439 --> 00:00:24,400
the compiler to figure out how it

10
00:00:22,199 --> 00:00:28,199
behaves under certain conditions for

11
00:00:24,400 --> 00:00:28,199
certain compilation options

