1
00:00:00,240 --> 00:00:03,439
all right now we need to pick another

2
00:00:01,839 --> 00:00:07,120
assembly instruction and I'm going to go

3
00:00:03,439 --> 00:00:10,080
ahead and pick this C

4
00:00:07,120 --> 00:00:13,839
sdsp so going back to that diagram from

5
00:00:10,080 --> 00:00:17,840
the book page 65 we have our C sdsp we

6
00:00:13,839 --> 00:00:22,600
move it up top we go CS ssss okay here's

7
00:00:17,840 --> 00:00:25,640
our first CS but those are CSL and CSR

8
00:00:22,600 --> 00:00:27,599
that's not it keep moving we got C

9
00:00:25,640 --> 00:00:32,759
nothing so we can choose that path cuz

10
00:00:27,599 --> 00:00:35,600
CF is not right C nothing s w oh no

11
00:00:32,759 --> 00:00:37,559
that's not what we want and CF nope

12
00:00:35,600 --> 00:00:41,039
that's not it and nothing over here as

13
00:00:37,559 --> 00:00:43,559
well so we got nothing

14
00:00:41,039 --> 00:00:45,640
here so there's actually a different

15
00:00:43,559 --> 00:00:48,239
diagram elsewhere in the book for the RV

16
00:00:45,640 --> 00:00:50,640
64c so instructions that are

17
00:00:48,239 --> 00:00:55,000
specifically only valid when you have

18
00:00:50,640 --> 00:01:00,320
64-bit compressed things so C sdsp move

19
00:00:55,000 --> 00:01:04,280
up here check ads nope CS nope C

20
00:01:00,320 --> 00:01:08,360
underscore for nothing s for store D for

21
00:01:04,280 --> 00:01:11,360
double word SD and SP using stack

22
00:01:08,360 --> 00:01:14,640
pointer check that is what we want so C

23
00:01:11,360 --> 00:01:17,640
sdsp is compressed store double word

24
00:01:14,640 --> 00:01:20,119
using stack pointer all right so that is

25
00:01:17,640 --> 00:01:22,400
going to then expand out to just a store

26
00:01:20,119 --> 00:01:23,680
double word so we have the notion

27
00:01:22,400 --> 00:01:25,400
already we've covered the assembly

28
00:01:23,680 --> 00:01:28,119
instruction store double word we know

29
00:01:25,400 --> 00:01:31,520
that that's a 64-bit thing for storing

30
00:01:28,119 --> 00:01:33,840
64-bit values into memory and it is red

31
00:01:31,520 --> 00:01:35,759
like all stores from left to right so it

32
00:01:33,840 --> 00:01:38,280
takes this 64-bit value and stores it

33
00:01:35,759 --> 00:01:40,600
out to memory specified by Stack pointer

34
00:01:38,280 --> 00:01:43,479
plus 8 and then go to that as an address

35
00:01:40,600 --> 00:01:46,079
and store it in memory so sdsp is

36
00:01:43,479 --> 00:01:48,880
somehow just specifically using the

37
00:01:46,079 --> 00:01:51,399
stack pointer as its destination so

38
00:01:48,880 --> 00:01:53,640
looking at this sdsp is compressed store

39
00:01:51,399 --> 00:01:56,799
double word using stack pointer we've

40
00:01:53,640 --> 00:01:59,119
got an rs2 that's our source to the

41
00:01:56,799 --> 00:02:01,560
source one being this SP it's always

42
00:01:59,119 --> 00:02:04,560
hard-coded to be SP and so we're taking

43
00:02:01,560 --> 00:02:08,759
rs2 that is the source and moving it to

44
00:02:04,560 --> 00:02:11,720
memory at SP plus unsigned or zero

45
00:02:08,759 --> 00:02:13,640
extended immediate 9 and that is going

46
00:02:11,720 --> 00:02:14,959
to be used as a memory address and we're

47
00:02:13,640 --> 00:02:18,040
going to store something there we're

48
00:02:14,959 --> 00:02:20,080
going to store rs2 to memory specified

49
00:02:18,040 --> 00:02:22,360
by that address now I should tell you

50
00:02:20,080 --> 00:02:25,239
that the documentation says that that

51
00:02:22,360 --> 00:02:26,920
immediate 9 is scaled by eight and that

52
00:02:25,239 --> 00:02:29,400
means the bottom three bits are going to

53
00:02:26,920 --> 00:02:31,519
be implicit zeros and so only six of the

54
00:02:29,400 --> 00:02:34,160
nine bit are actually in the assembly

55
00:02:31,519 --> 00:02:36,920
instruction and again stores always are

56
00:02:34,160 --> 00:02:39,080
read by a human moving this value from

57
00:02:36,920 --> 00:02:41,280
the left side to the right side as a

58
00:02:39,080 --> 00:02:44,440
memory address so if we looked at the

59
00:02:41,280 --> 00:02:45,959
actual data sheet we would see that it

60
00:02:44,440 --> 00:02:47,519
says this computes the effect of address

61
00:02:45,959 --> 00:02:49,560
by adding the zero extended offset

62
00:02:47,519 --> 00:02:51,239
scaled by eight to the stack pointer and

63
00:02:49,560 --> 00:02:53,519
then more importantly if we look at the

64
00:02:51,239 --> 00:02:56,800
actual immediate we can see that while

65
00:02:53,519 --> 00:02:59,159
it has a bit 8 that is the highest bit

66
00:02:56,800 --> 00:03:01,239
there is no bit two there is no bit one

67
00:02:59,159 --> 00:03:04,239
there is no bit bit Z so there's only

68
00:03:01,239 --> 00:03:07,519
six bits and the maximum bit is bit8 so

69
00:03:04,239 --> 00:03:09,280
it must be a 9 bit total immediate but 0

70
00:03:07,519 --> 00:03:11,720
1 and two are unspecified they're

71
00:03:09,280 --> 00:03:13,640
implicitly zero that means it is scaled

72
00:03:11,720 --> 00:03:16,720
by eight which is only multiples of

73
00:03:13,640 --> 00:03:20,080
eight so you could have a value like 8 *

74
00:03:16,720 --> 00:03:22,959
0 and you'd get 0er 8 * 1 you could have

75
00:03:20,080 --> 00:03:24,519
one in this bit three right here but

76
00:03:22,959 --> 00:03:30,120
then the other bits would be zero so you

77
00:03:24,519 --> 00:03:32,640
would have 8 2 * 8 = hex 10 3 * 8 = hex

78
00:03:30,120 --> 00:03:34,319
18 and so forth so just multiples of

79
00:03:32,640 --> 00:03:37,120
eight so in general anytime you're

80
00:03:34,319 --> 00:03:39,319
looking at the actual definitions in the

81
00:03:37,120 --> 00:03:41,519
data sheet you can just kind of look at

82
00:03:39,319 --> 00:03:43,720
like what is the least significant bit

83
00:03:41,519 --> 00:03:45,799
specified so here for this thing the

84
00:03:43,720 --> 00:03:47,720
least significant bit is two which means

85
00:03:45,799 --> 00:03:49,720
one and zero are zero here the least

86
00:03:47,720 --> 00:03:52,239
significant bit thing is four which

87
00:03:49,720 --> 00:03:53,959
means it's got the bottom four bits are

88
00:03:52,239 --> 00:03:56,439
all zero so this means this one must be

89
00:03:53,959 --> 00:03:58,480
scaled by 16 this one must be scaled by

90
00:03:56,439 --> 00:04:00,439
four even before we actually look at the

91
00:03:58,480 --> 00:04:02,599
definitions we can figure that out just

92
00:04:00,439 --> 00:04:04,599
by this encoding format that they use to

93
00:04:02,599 --> 00:04:06,040
talk about how the immediate is encoded

94
00:04:04,599 --> 00:04:09,000
but there is an interesting question

95
00:04:06,040 --> 00:04:12,239
here and that is why is C sdsp even

96
00:04:09,000 --> 00:04:16,280
needed if we look at the other things

97
00:04:12,239 --> 00:04:19,120
there is a CSD it's just a store dword

98
00:04:16,280 --> 00:04:21,040
like why do we need a store dword to the

99
00:04:19,120 --> 00:04:23,880
stack pointer when there is just a store

100
00:04:21,040 --> 00:04:27,960
dword it's encoded as store dword rs2

101
00:04:23,880 --> 00:04:29,680
prime unsigned immediate 8 and rs1 prime

102
00:04:27,960 --> 00:04:32,520
so you would think that you could just

103
00:04:29,680 --> 00:04:34,840
set rs1 prime to stack pointer and be

104
00:04:32,520 --> 00:04:37,479
done with it right like here is our path

105
00:04:34,840 --> 00:04:40,720
through that picture telling us that

106
00:04:37,479 --> 00:04:42,680
CSD nothing is a thing so why do we need

107
00:04:40,720 --> 00:04:44,240
stack pointer specific one well the

108
00:04:42,680 --> 00:04:46,199
answer is because when we look at those

109
00:04:44,240 --> 00:04:48,120
encodings and we see that you know it's

110
00:04:46,199 --> 00:04:50,440
only 16 bits total which means there's

111
00:04:48,120 --> 00:04:53,600
not much space we see that registers

112
00:04:50,440 --> 00:04:56,400
like rs1 prime and rs2 prime they only

113
00:04:53,600 --> 00:04:58,639
actually have three bits to encode a

114
00:04:56,400 --> 00:05:00,800
particular register and that's why they

115
00:04:58,639 --> 00:05:05,199
have been restricted to only a subset of

116
00:05:00,800 --> 00:05:07,919
the registers specifically X8 X9 X10 X11

117
00:05:05,199 --> 00:05:10,120
all the way through X15 so basically in

118
00:05:07,919 --> 00:05:12,280
the assembly instructions you only get

119
00:05:10,120 --> 00:05:14,680
three bits to say where it's going to go

120
00:05:12,280 --> 00:05:17,600
and that means this form is not even

121
00:05:14,680 --> 00:05:19,600
capable of specifying the stack pointer

122
00:05:17,600 --> 00:05:23,000
because if you recall back to the

123
00:05:19,600 --> 00:05:26,000
register section x0 was the hardcoded to

124
00:05:23,000 --> 00:05:28,560
zero x1 is the return address and x2 is

125
00:05:26,000 --> 00:05:31,360
the stack pointer so x2 is not even an

126
00:05:28,560 --> 00:05:33,720
option so that is why we need this c

127
00:05:31,360 --> 00:05:36,639
sdsp means that they recognize that

128
00:05:33,720 --> 00:05:39,039
there is very frequent rights to memory

129
00:05:36,639 --> 00:05:40,960
that is stack pointer relative things

130
00:05:39,039 --> 00:05:42,560
like that storing of the frame pointer

131
00:05:40,960 --> 00:05:44,960
for instance that we know is going to

132
00:05:42,560 --> 00:05:46,800
occur that is going to be at some offset

133
00:05:44,960 --> 00:05:49,880
from the stack pointer and so they made

134
00:05:46,800 --> 00:05:53,120
an instruction specifically to hardcode

135
00:05:49,880 --> 00:05:55,199
in a destination of a stack pointer so a

136
00:05:53,120 --> 00:05:57,639
stack pointer plus something and that

137
00:05:55,199 --> 00:05:59,479
gave them the ability to free up some

138
00:05:57,639 --> 00:06:02,639
extra bits to make it so that you could

139
00:05:59,479 --> 00:06:05,720
could specify any register all five bits

140
00:06:02,639 --> 00:06:07,599
all 32-bit registers any register can

141
00:06:05,720 --> 00:06:10,000
now be stored to a stack pointer

142
00:06:07,599 --> 00:06:11,479
relative Thing by creating this special

143
00:06:10,000 --> 00:06:13,639
instruction that is stack pointer

144
00:06:11,479 --> 00:06:15,520
specific and with that we've figured out

145
00:06:13,639 --> 00:06:18,639
what's going on with this compressed

146
00:06:15,520 --> 00:06:22,120
store dword to a stack pointer relative

147
00:06:18,639 --> 00:06:25,280
address so here it is store s0 which we

148
00:06:22,120 --> 00:06:27,280
know is the frame pointer to SP plus 8

149
00:06:25,280 --> 00:06:30,800
and it just is that you have to use the

150
00:06:27,280 --> 00:06:34,319
sdsp instead of SD for doing this to the

151
00:06:30,800 --> 00:06:34,319
stack pointer register

