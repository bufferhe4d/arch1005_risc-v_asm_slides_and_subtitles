1
00:00:01,360 --> 00:00:04,759
so if you step through everything you

2
00:00:02,800 --> 00:00:07,279
should see something like this but if we

3
00:00:04,759 --> 00:00:09,920
specifically step through main plus 44

4
00:00:07,279 --> 00:00:11,799
the last store to memory then this is

5
00:00:09,920 --> 00:00:14,200
where everything would be frame pointer

6
00:00:11,799 --> 00:00:17,119
here stack pointer here and we've got

7
00:00:14,200 --> 00:00:19,840
our baded deadbeat affected oddballs and

8
00:00:17,119 --> 00:00:21,920
leafless bold face in this order which

9
00:00:19,840 --> 00:00:24,640
happens to again be the opposite order

10
00:00:21,920 --> 00:00:26,480
of Declaration in the SE source coat but

11
00:00:24,640 --> 00:00:28,760
now once again there has been a

12
00:00:26,480 --> 00:00:32,000
reintroduction at the top of the stack

13
00:00:28,760 --> 00:00:34,040
of 8 bytes of initialized data so if

14
00:00:32,000 --> 00:00:36,040
this was our two long Longs affected

15
00:00:34,040 --> 00:00:38,079
oddballs leafless bold face this is our

16
00:00:36,040 --> 00:00:41,719
three long Longs they were sort of

17
00:00:38,079 --> 00:00:45,000
pushed down by 8 and the new value was

18
00:00:41,719 --> 00:00:47,280
placed up here at 8 f8 where the other

19
00:00:45,000 --> 00:00:49,239
ones previously were so okay maybe

20
00:00:47,280 --> 00:00:51,760
you're starting to see the pattern but

21
00:00:49,239 --> 00:00:53,440
if not here's all the various versions

22
00:00:51,760 --> 00:00:56,039
of things that we've seen thus far plus

23
00:00:53,440 --> 00:00:58,120
an extra bonus four long Longs so if you

24
00:00:56,039 --> 00:01:00,039
want to go ahead and create a new

25
00:00:58,120 --> 00:01:02,840
variant or the codee has already been

26
00:01:00,039 --> 00:01:04,879
created for you with Telltale obstacle

27
00:01:02,840 --> 00:01:08,400
you can see this is how forong longs

28
00:01:04,879 --> 00:01:10,640
would behave so Clues everywhere what is

29
00:01:08,400 --> 00:01:13,560
the compiler doing how does it seem to

30
00:01:10,640 --> 00:01:16,200
be behaving and what we can see is that

31
00:01:13,560 --> 00:01:18,400
it seems like every time it's going to

32
00:01:16,200 --> 00:01:22,799
have local variables it's going to

33
00:01:18,400 --> 00:01:26,360
allocate he10 or 16 byte chunks so here

34
00:01:22,799 --> 00:01:29,680
it allocated 16 bytes but it only used

35
00:01:26,360 --> 00:01:31,799
eight for two 4 byte values 2 ins and

36
00:01:29,680 --> 00:01:34,640
then left the other eight uninitialized

37
00:01:31,799 --> 00:01:37,399
when you had 16 bytes worth of data so

38
00:01:34,640 --> 00:01:40,360
two 8 by values affected Oddball and

39
00:01:37,399 --> 00:01:42,399
leafless bolt face then it allocated 16

40
00:01:40,360 --> 00:01:44,520
bytes and it perfectly filled in 16

41
00:01:42,399 --> 00:01:49,479
bytes on the other hand when you had

42
00:01:44,520 --> 00:01:52,479
three 8 by values it allocated two 16 by

43
00:01:49,479 --> 00:01:54,799
allocations so 32 bytes worth of space

44
00:01:52,479 --> 00:01:56,840
but it only had 24 bytes worth of data

45
00:01:54,799 --> 00:01:58,439
and therefore it left it uninitialized

46
00:01:56,840 --> 00:02:00,479
and the final data point which really

47
00:01:58,439 --> 00:02:03,200
hits at home is the fact that if we had

48
00:02:00,479 --> 00:02:05,479
four long Longs which is exactly 32

49
00:02:03,200 --> 00:02:08,239
bytes worth of data then it allocated

50
00:02:05,479 --> 00:02:12,040
exactly 32 so it seems to be allocating

51
00:02:08,239 --> 00:02:14,400
in 16 byte chunks hex 10 at a time

52
00:02:12,040 --> 00:02:17,160
overallocated if necessary and leaving

53
00:02:14,400 --> 00:02:20,400
the unused memory untouched so that's

54
00:02:17,160 --> 00:02:23,879
our conclusion but why though well the

55
00:02:20,400 --> 00:02:25,640
answer comes from the ABI documentation

56
00:02:23,879 --> 00:02:27,760
and so it specifically says the stack

57
00:02:25,640 --> 00:02:29,120
grows downward towards lower addresses

58
00:02:27,760 --> 00:02:31,360
well we already got that and we just

59
00:02:29,120 --> 00:02:33,720
took that as a premise a starting

60
00:02:31,360 --> 00:02:37,680
premise but it says the stack pointer

61
00:02:33,720 --> 00:02:40,800
shall be aligned to a 128bit boundary

62
00:02:37,680 --> 00:02:43,040
that is a 16 byte boundary upon

63
00:02:40,800 --> 00:02:44,879
procedure entry so the first thing is

64
00:02:43,040 --> 00:02:46,879
that when you come into a function it

65
00:02:44,879 --> 00:02:50,239
better be aligned the stack pointer

66
00:02:46,879 --> 00:02:53,480
better be a multiple of hex 10 128 bits

67
00:02:50,239 --> 00:02:56,720
16 bytes hex 10 so you need to start on

68
00:02:53,480 --> 00:02:59,120
a hex1 boundary and it further says in

69
00:02:56,720 --> 00:03:01,440
the standard AI the stack pointer must

70
00:02:59,120 --> 00:03:04,159
remain lined throughout the procedure

71
00:03:01,440 --> 00:03:05,680
execution and that's the kicker here so

72
00:03:04,159 --> 00:03:08,799
if you're going to change the stack

73
00:03:05,680 --> 00:03:12,120
pointer you must change it in aligned or

74
00:03:08,799 --> 00:03:14,239
multiples of hex1 so two large variable

75
00:03:12,120 --> 00:03:16,519
allocations this mystery that we

76
00:03:14,239 --> 00:03:19,280
couldn't really figure out if we pull up

77
00:03:16,519 --> 00:03:23,360
the hood it turns out to be a 16 by

78
00:03:19,280 --> 00:03:28,239
alignment ABI requirement so mystery

79
00:03:23,360 --> 00:03:29,799
solved boom mystery solved perfect so

80
00:03:28,239 --> 00:03:31,959
that's why it's allocating over

81
00:03:29,799 --> 00:03:33,920
allocating seemingly over allocating

82
00:03:31,959 --> 00:03:35,720
sing uh for a single local variable

83
00:03:33,920 --> 00:03:38,480
right or even two local variables if it

84
00:03:35,720 --> 00:03:41,720
was two in size variables basic point is

85
00:03:38,480 --> 00:03:43,959
it's trying to stay aligned to hx10

86
00:03:41,720 --> 00:03:46,000
granularity and so that other mystery

87
00:03:43,959 --> 00:03:48,159
monster that has been plaguing us the

88
00:03:46,000 --> 00:03:51,159
overly large frame pointer space

89
00:03:48,159 --> 00:03:53,439
allocations could also be a 16 BYT

90
00:03:51,159 --> 00:03:56,560
alignment ABI requirement right so we

91
00:03:53,439 --> 00:03:59,319
saw in just return that it allocated

92
00:03:56,560 --> 00:04:01,920
hex1 16 bytes for the frame pointer

93
00:03:59,319 --> 00:04:03,920
which is stored but it was only using

94
00:04:01,920 --> 00:04:06,040
eight it's only using eight for the

95
00:04:03,920 --> 00:04:08,400
actual frame pointer but if it wants to

96
00:04:06,040 --> 00:04:10,519
use eight it needs to keep the stack

97
00:04:08,400 --> 00:04:13,920
pointer aligned that means it must move

98
00:04:10,519 --> 00:04:16,280
the stack pointer down by xx10 and with

99
00:04:13,920 --> 00:04:18,120
that another mystery solved good job

100
00:04:16,280 --> 00:04:21,160
gang but it seems that the side ey

101
00:04:18,120 --> 00:04:23,880
monkey is side eyeing these Solutions

102
00:04:21,160 --> 00:04:25,880
and wonders if they're both just doe to

103
00:04:23,880 --> 00:04:27,840
alignment the over allocation for frame

104
00:04:25,880 --> 00:04:30,199
pointers and the over allocation for

105
00:04:27,840 --> 00:04:32,560
local variables well then in single

106
00:04:30,199 --> 00:04:34,759
local variable we saw eight bytes for

107
00:04:32,560 --> 00:04:38,280
the frame pointer and four bytes for the

108
00:04:34,759 --> 00:04:39,800
single int why couldn't it just use hex1

109
00:04:38,280 --> 00:04:41,520
allocation it would keep the stack

110
00:04:39,800 --> 00:04:43,479
pointer aligned but there wouldn't be

111
00:04:41,520 --> 00:04:46,000
that weird and annoying uninitialized

112
00:04:43,479 --> 00:04:48,320
space there well my answer to that is

113
00:04:46,000 --> 00:04:50,479
that yes perhaps it could have done that

114
00:04:48,320 --> 00:04:52,639
but remember first of all that this is

115
00:04:50,479 --> 00:04:55,240
optimization level zero for that single

116
00:04:52,639 --> 00:04:58,360
local variable o z so we explicitly told

117
00:04:55,240 --> 00:05:00,639
it don't be optimal the next thing is

118
00:04:58,360 --> 00:05:03,720
well could it compress down the space

119
00:05:00,639 --> 00:05:05,800
further and the answer is perhaps let's

120
00:05:03,720 --> 00:05:07,880
come back to that later let's look after

121
00:05:05,800 --> 00:05:09,520
we learn a little bit more about other

122
00:05:07,880 --> 00:05:12,199
things that could be stored on the stack

123
00:05:09,520 --> 00:05:15,600
but I will just also ask the question to

124
00:05:12,199 --> 00:05:17,880
what end right so is the compiler

125
00:05:15,600 --> 00:05:20,160
expecting to deal with situation where

126
00:05:17,880 --> 00:05:22,199
it really needs to save space on the

127
00:05:20,160 --> 00:05:24,960
stack and it's exhausting all space in

128
00:05:22,199 --> 00:05:27,680
the stack perhaps in some very specific

129
00:05:24,960 --> 00:05:29,560
contexts but perhaps not on systems

130
00:05:27,680 --> 00:05:32,080
where you can simply add more RAM and

131
00:05:29,560 --> 00:05:34,199
and consequently get more stack space so

132
00:05:32,080 --> 00:05:36,160
the short answer to this question is

133
00:05:34,199 --> 00:05:37,720
perhaps that could be done perhaps you

134
00:05:36,160 --> 00:05:42,400
could compress everything down to a

135
00:05:37,720 --> 00:05:44,080
single hex uh one Z allocation but after

136
00:05:42,400 --> 00:05:45,800
we learn a little bit more about other

137
00:05:44,080 --> 00:05:48,800
stuff that's going on the stack we will

138
00:05:45,800 --> 00:05:51,560
see why perhaps GCC decides not to do

139
00:05:48,800 --> 00:05:51,560
that

