1
00:00:00,080 --> 00:00:03,520
all right no hidden assembly

2
00:00:01,400 --> 00:00:07,160
instructions here so all that we have

3
00:00:03,520 --> 00:00:09,240
for a takeaway is the fact that S S and

4
00:00:07,160 --> 00:00:11,719
S are for those times when the compiler

5
00:00:09,240 --> 00:00:14,080
doesn't know at compile time how much it

6
00:00:11,719 --> 00:00:16,000
needs to shift by but otherwise they're

7
00:00:14,080 --> 00:00:18,279
the same old same old in terms of

8
00:00:16,000 --> 00:00:20,439
logical left shifting right shifting

9
00:00:18,279 --> 00:00:22,880
logical and right shifting

10
00:00:20,439 --> 00:00:26,279
arithmetic so picked up our new assembly

11
00:00:22,880 --> 00:00:28,199
instructions Sil surl and sraw and

12
00:00:26,279 --> 00:00:30,759
there's no equivalent separate ones in

13
00:00:28,199 --> 00:00:34,160
64-bit land because they're encoded

14
00:00:30,759 --> 00:00:37,079
exactly the same on our diagram we have

15
00:00:34,160 --> 00:00:40,840
three new things nicely slotting in next

16
00:00:37,079 --> 00:00:40,840
to their immediate cousins

