1
00:00:00,040 --> 00:00:04,799
so the takeaway from this shift example

2
00:00:02,000 --> 00:00:07,279
6 miv was that when there's

3
00:00:04,799 --> 00:00:09,800
multiplication or division but it's by a

4
00:00:07,279 --> 00:00:11,920
power of two so if the value being

5
00:00:09,800 --> 00:00:13,759
multiplied or divided is a power of two

6
00:00:11,920 --> 00:00:16,320
then the compiler will greatly prefer to

7
00:00:13,759 --> 00:00:18,720
use shift instructions because it is

8
00:00:16,320 --> 00:00:21,400
generally much easier for the hardware

9
00:00:18,720 --> 00:00:23,119
to just shift bits left shift them right

10
00:00:21,400 --> 00:00:25,119
than to calculate the correct value for

11
00:00:23,119 --> 00:00:27,840
a multiplication and a miscellaneous

12
00:00:25,119 --> 00:00:32,279
aside is that we see that the a2i got

13
00:00:27,840 --> 00:00:32,279
turned into a string to Long yeah

