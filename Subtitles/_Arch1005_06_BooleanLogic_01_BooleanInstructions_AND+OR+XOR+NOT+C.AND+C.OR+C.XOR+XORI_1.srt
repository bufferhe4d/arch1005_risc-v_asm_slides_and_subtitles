1
00:00:00,040 --> 00:00:04,400
now it's time to see how Boolean

2
00:00:01,800 --> 00:00:06,879
operations manifest in Risk 5 assembly

3
00:00:04,400 --> 00:00:08,920
so we've got soda we've got coffee te

4
00:00:06,879 --> 00:00:11,200
total taboo I'm not saying it's about

5
00:00:08,920 --> 00:00:14,480
Mormons but it's about Mormons so we've

6
00:00:11,200 --> 00:00:17,439
also got the Boolean or the Boolean and

7
00:00:14,480 --> 00:00:21,000
the Boolean not and the Boolean exor the

8
00:00:17,439 --> 00:00:24,199
carrot here is an exor so compiling that

9
00:00:21,000 --> 00:00:26,840
and we of course end up with and not or

10
00:00:24,199 --> 00:00:29,400
an exor the exact Boolean operations we

11
00:00:26,840 --> 00:00:31,640
just put into our code so what is and

12
00:00:29,400 --> 00:00:33,840
well that is the bitwise and and just as

13
00:00:31,640 --> 00:00:37,559
a reminder here's your truth tables in

14
00:00:33,840 --> 00:00:40,079
the upper right corner 0 and 0 is 0 0

15
00:00:37,559 --> 00:00:43,039
and 1 is zero one and zero is zero and

16
00:00:40,079 --> 00:00:45,680
only when one and one will you get an

17
00:00:43,039 --> 00:00:48,000
one as a result so and takes three

18
00:00:45,680 --> 00:00:49,960
operands the register source one

19
00:00:48,000 --> 00:00:52,800
register source 2 and register

20
00:00:49,960 --> 00:00:56,480
destination it does register source one

21
00:00:52,800 --> 00:00:58,760
bitwise and with register source to so

22
00:00:56,480 --> 00:01:01,559
goes down the line and does the and

23
00:00:58,760 --> 00:01:05,080
operation on every single bit places the

24
00:01:01,559 --> 00:01:06,880
result into register destination so the

25
00:01:05,080 --> 00:01:09,680
C binary operator as we already

26
00:01:06,880 --> 00:01:11,759
indicated is the Amper sand not the

27
00:01:09,680 --> 00:01:15,040
double Amper sand because that is a

28
00:01:11,759 --> 00:01:16,880
logical and not a bitwise and so let's

29
00:01:15,040 --> 00:01:20,960
say that we are doing the instruction

30
00:01:16,880 --> 00:01:23,799
and a5 a5 a4 then let's say that the

31
00:01:20,960 --> 00:01:27,360
initial value of a5 before the end

32
00:01:23,799 --> 00:01:29,439
operation is staba daaba dudes and at

33
00:01:27,360 --> 00:01:31,280
this point I have to say that you know

34
00:01:29,439 --> 00:01:34,240
I'm going introduce a couple of staba

35
00:01:31,280 --> 00:01:36,159
daaba dudes here but these particular

36
00:01:34,240 --> 00:01:38,320
characters are not the ones I want these

37
00:01:36,159 --> 00:01:40,479
characters are from this particular RS

38
00:01:38,320 --> 00:01:42,880
Technica article where they were talking

39
00:01:40,479 --> 00:01:45,119
about AI generated characters from

40
00:01:42,880 --> 00:01:47,880
Facebook stickers that would make

41
00:01:45,119 --> 00:01:50,640
inappropriate copyrighted instances of

42
00:01:47,880 --> 00:01:53,680
people now for the stabba deba dudes I

43
00:01:50,640 --> 00:01:56,799
really wanted to have the Flintstones

44
00:01:53,680 --> 00:01:59,200
you know Fred and Barney but at the time

45
00:01:56,799 --> 00:02:00,960
of this class creation of course this

46
00:01:59,200 --> 00:02:03,560
capability of has already been disabled

47
00:02:00,960 --> 00:02:05,600
in Facebook sticker creation and and I

48
00:02:03,560 --> 00:02:07,200
don't have a Facebook account anyways

49
00:02:05,600 --> 00:02:09,959
and although there are other AI

50
00:02:07,200 --> 00:02:12,200
generators that could probably generate

51
00:02:09,959 --> 00:02:14,200
some staba daaba dudes for me I don't

52
00:02:12,200 --> 00:02:16,080
really want to go to the effort now if

53
00:02:14,200 --> 00:02:18,840
any students want to make me some staba

54
00:02:16,080 --> 00:02:21,519
daaba dudes later in specifically this

55
00:02:18,840 --> 00:02:23,519
sticker format nice cute cuddly format

56
00:02:21,519 --> 00:02:25,160
not just like a larger thing if you want

57
00:02:23,519 --> 00:02:27,920
to make that for me I will happily

58
00:02:25,160 --> 00:02:30,200
re-record this section but for now these

59
00:02:27,920 --> 00:02:32,000
are going to be our staba ABA dudes and

60
00:02:30,200 --> 00:02:35,800
we are going to perform the and

61
00:02:32,000 --> 00:02:39,720
operation between the stab dudes and Deo

62
00:02:35,800 --> 00:02:41,560
deao and the net result of that a5 after

63
00:02:39,720 --> 00:02:46,159
this operation is that you go down the

64
00:02:41,560 --> 00:02:50,000
line bitwise zero and one zero one and

65
00:02:46,159 --> 00:02:52,360
one one and so forth so that's what the

66
00:02:50,000 --> 00:02:54,840
computer is doing behind the scenes so

67
00:02:52,360 --> 00:02:56,680
then the or instruction same idea you've

68
00:02:54,840 --> 00:02:59,440
got your truth table here to remind you

69
00:02:56,680 --> 00:03:02,640
if you need it or is true whenever

70
00:02:59,440 --> 00:03:04,879
either of the operands is true so the or

71
00:03:02,640 --> 00:03:07,280
instruction takes two sources source one

72
00:03:04,879 --> 00:03:09,080
source two performs a bitwise ore and

73
00:03:07,280 --> 00:03:12,200
places the result into the destination

74
00:03:09,080 --> 00:03:15,120
register and again the single pipe is

75
00:03:12,200 --> 00:03:19,040
the binary ore whereas the double pipe

76
00:03:15,120 --> 00:03:21,879
is the logical or also called the bar or

77
00:03:19,040 --> 00:03:24,239
double bar so if you have your staba

78
00:03:21,879 --> 00:03:26,959
daad dudes and dabad daao again and

79
00:03:24,239 --> 00:03:29,239
you're doing an or operation then the

80
00:03:26,959 --> 00:03:35,319
result is just go down the line zero or

81
00:03:29,239 --> 00:03:37,319
one one one or one one Z or Z zero so

82
00:03:35,319 --> 00:03:40,080
computer just does all the binary math

83
00:03:37,319 --> 00:03:42,959
behind the scenes then the xor operation

84
00:03:40,080 --> 00:03:45,959
exor's truth table is true when one or

85
00:03:42,959 --> 00:03:47,720
the other operand is one but not both

86
00:03:45,959 --> 00:03:50,799
and not neither so you have to have

87
00:03:47,720 --> 00:03:54,159
exactly one one operand in order to get

88
00:03:50,799 --> 00:03:58,120
a one out of X or exclusive or so the

89
00:03:54,159 --> 00:04:01,599
carrot is the exor operator in C and

90
00:03:58,120 --> 00:04:04,959
there is no you know logic iCal exor so

91
00:04:01,599 --> 00:04:09,599
xoring a5 with a4 and placing the result

92
00:04:04,959 --> 00:04:12,280
into a5 stab daud staba Deo and we get

93
00:04:09,599 --> 00:04:15,360
some random garbage but again it's just

94
00:04:12,280 --> 00:04:19,079
zero X or one yes that is one one X or

95
00:04:15,360 --> 00:04:22,759
one no that is zero and so forth and the

96
00:04:19,079 --> 00:04:25,440
knot bitwise knot is the TIY and so it's

97
00:04:22,759 --> 00:04:27,600
going to have only two operands the

98
00:04:25,440 --> 00:04:29,960
source which shall have all of its bits

99
00:04:27,600 --> 00:04:30,880
flipped and the result will be placed

100
00:04:29,960 --> 00:04:33,440
into the

101
00:04:30,880 --> 00:04:35,320
destination and truth table for not is

102
00:04:33,440 --> 00:04:37,840
just flip the bits if it was zero make

103
00:04:35,320 --> 00:04:42,039
it a one if it was one make it a zero so

104
00:04:37,840 --> 00:04:44,080
that is again the TIY not the uh bang or

105
00:04:42,039 --> 00:04:49,000
exclamation point because that is a

106
00:04:44,080 --> 00:04:50,440
logical knot so if a5 had 81 AA and some

107
00:04:49,000 --> 00:04:52,759
sort of different thing why are we not

108
00:04:50,440 --> 00:04:55,400
having staba daaba dudes because if you

109
00:04:52,759 --> 00:04:58,560
not that you get tesselated toads

110
00:04:55,400 --> 00:05:01,560
tesselated toads tesselated

111
00:04:58,560 --> 00:05:01,560
toads

112
00:05:04,600 --> 00:05:09,560
likes me some tesselated

113
00:05:06,320 --> 00:05:11,600
toads and now Hypnotoad commands you to

114
00:05:09,560 --> 00:05:13,560
step through the assembly and check your

115
00:05:11,600 --> 00:05:15,080
understanding you can create a stack

116
00:05:13,560 --> 00:05:18,680
diagram if you want but it's not

117
00:05:15,080 --> 00:05:18,680
strictly necessary

