1
00:00:00,160 --> 00:00:05,839
so this is what the stack looks like if

2
00:00:02,240 --> 00:00:08,160
you stepped through function plus 6 and

3
00:00:05,839 --> 00:00:11,400
it's worth noting here that if this is

4
00:00:08,160 --> 00:00:13,679
main's frame this is functions frame Z

5
00:00:11,400 --> 00:00:16,400
is still uninitialized so this area was

6
00:00:13,679 --> 00:00:17,960
sort of notionally reserved for Z but at

7
00:00:16,400 --> 00:00:19,720
this point in the code execution we

8
00:00:17,960 --> 00:00:21,800
don't see anything filled in there

9
00:00:19,720 --> 00:00:24,560
because the Z is set when you return

10
00:00:21,800 --> 00:00:26,519
from func so it's allocated but

11
00:00:24,560 --> 00:00:28,880
uninitialized however when you get down

12
00:00:26,519 --> 00:00:31,720
into here you do see that for whatever

13
00:00:28,880 --> 00:00:34,200
reason func decides to store its

14
00:00:31,720 --> 00:00:35,760
argument to the function on the stack

15
00:00:34,200 --> 00:00:38,000
stores it out and then it immediately

16
00:00:35,760 --> 00:00:39,559
loads it again being again one of those

17
00:00:38,000 --> 00:00:41,840
sort of pointless things that seems to

18
00:00:39,559 --> 00:00:43,239
happen from unoptimized assembly but

19
00:00:41,840 --> 00:00:45,440
this is what you see when you're inside

20
00:00:43,239 --> 00:00:48,239
a func and then I'll just point out that

21
00:00:45,440 --> 00:00:50,840
if you return back out of func this

22
00:00:48,239 --> 00:00:53,120
stuff becomes undefined because it's now

23
00:00:50,840 --> 00:00:56,000
lower than the stack pointer it's pass

24
00:00:53,120 --> 00:00:58,760
to the stack top and at that point the

25
00:00:56,000 --> 00:01:00,800
truncated version of faceless cold Cola

26
00:00:58,760 --> 00:01:02,879
is going to be placed into z z is going

27
00:01:00,800 --> 00:01:04,600
to be at the upper four bytes as we've

28
00:01:02,879 --> 00:01:06,920
seen throughout that when there's a

29
00:01:04,600 --> 00:01:09,520
single int it gets the upper four bytes

30
00:01:06,920 --> 00:01:11,400
of a 8 by allocation but let's go ahead

31
00:01:09,520 --> 00:01:13,680
and look at this code a little more

32
00:01:11,400 --> 00:01:16,600
carefully uh just to highlight some

33
00:01:13,680 --> 00:01:19,680
things we saw that the constant faceless

34
00:01:16,600 --> 00:01:23,200
cold Cola was loaded into the argument

35
00:01:19,680 --> 00:01:26,040
five using the wepc plus ad wepc plus

36
00:01:23,200 --> 00:01:27,759
Zer means you're just get PC into a5 a5

37
00:01:26,040 --> 00:01:31,000
plus a constant means you get some

38
00:01:27,759 --> 00:01:33,280
hardcoded address where presum ably this

39
00:01:31,000 --> 00:01:35,600
64-bit value is stored right after our

40
00:01:33,280 --> 00:01:36,640
code somewhere and then loaded into a5

41
00:01:35,600 --> 00:01:40,000
and then

42
00:01:36,640 --> 00:01:42,880
a5 and then sorry next is going to be a5

43
00:01:40,000 --> 00:01:45,600
into a0 and so this is the key thing

44
00:01:42,880 --> 00:01:48,880
that a z is the argument zero it's the

45
00:01:45,600 --> 00:01:51,719
zeroth argument to the function func so

46
00:01:48,880 --> 00:01:55,520
anytime you see things setting up a z a1

47
00:01:51,719 --> 00:01:57,960
a2 right before a call or a jaw to a

48
00:01:55,520 --> 00:02:00,000
function then that means they're setting

49
00:01:57,960 --> 00:02:02,159
it up to be used as an argument to that

50
00:02:00,000 --> 00:02:04,680
function then when we jaw into the

51
00:02:02,159 --> 00:02:07,079
function we have you know typical frame

52
00:02:04,680 --> 00:02:08,399
pointer setup typical function prologue

53
00:02:07,079 --> 00:02:11,160
but then there's that seemingly

54
00:02:08,399 --> 00:02:13,360
pointless storage of a z which is the

55
00:02:11,160 --> 00:02:15,720
argument store it out to memory but then

56
00:02:13,360 --> 00:02:17,440
immediately pull it back in from memory

57
00:02:15,720 --> 00:02:19,720
and then there's this extra sign

58
00:02:17,440 --> 00:02:22,760
extension but because this is a sign

59
00:02:19,720 --> 00:02:25,280
extension word that means this 64-bit

60
00:02:22,760 --> 00:02:27,360
value which is faceless called Cola is

61
00:02:25,280 --> 00:02:30,959
going to be truncated down to a word

62
00:02:27,360 --> 00:02:33,160
size a 32-bit value just old Cola and

63
00:02:30,959 --> 00:02:35,319
then sign extended into a5 so all of a

64
00:02:33,160 --> 00:02:37,720
sudden we just lost the upper 32 bits

65
00:02:35,319 --> 00:02:40,239
they become just fs and so now it's just

66
00:02:37,720 --> 00:02:42,519
going to be this integer type value and

67
00:02:40,239 --> 00:02:45,800
then after that we see that integer

68
00:02:42,519 --> 00:02:48,400
sized value being stored into a zero so

69
00:02:45,800 --> 00:02:50,319
that is the return value so as we're

70
00:02:48,400 --> 00:02:53,519
returning out of a function placement

71
00:02:50,319 --> 00:02:56,879
into a z or a1 should be construed to be

72
00:02:53,519 --> 00:02:59,440
the code loading something into a return

73
00:02:56,879 --> 00:03:01,840
register and then when you return back

74
00:02:59,440 --> 00:03:03,920
to main you're going to see the sequence

75
00:03:01,840 --> 00:03:07,040
the use of the a z which is the return

76
00:03:03,920 --> 00:03:09,080
value it's moved into a5 but then it's

77
00:03:07,040 --> 00:03:10,680
stored out to memory and then it's

78
00:03:09,080 --> 00:03:12,400
pulled back into memory well the store

79
00:03:10,680 --> 00:03:14,480
out to memory that is going to be the

80
00:03:12,400 --> 00:03:16,239
storage to the Z variable and the

81
00:03:14,480 --> 00:03:17,840
loading it back into the exact same

82
00:03:16,239 --> 00:03:20,920
register it's already in that's just

83
00:03:17,840 --> 00:03:23,760
typical unoptimized code and then

84
00:03:20,920 --> 00:03:26,200
storing that a5 into a z is further

85
00:03:23,760 --> 00:03:28,280
making it a return value for returning

86
00:03:26,200 --> 00:03:30,720
back out of main so basically in this

87
00:03:28,280 --> 00:03:33,400
code we saw the use of a z as an

88
00:03:30,720 --> 00:03:35,640
argument register going into a function

89
00:03:33,400 --> 00:03:38,000
we saw the use of a z as the return

90
00:03:35,640 --> 00:03:40,439
value coming back out we saw some un

91
00:03:38,000 --> 00:03:42,959
initial unoptimized code doing you know

92
00:03:40,439 --> 00:03:45,879
the typical unoptimized unnecessary

93
00:03:42,959 --> 00:03:47,720
things but yes this is the basics of

94
00:03:45,879 --> 00:03:50,879
calling into a function it's the

95
00:03:47,720 --> 00:03:53,120
placement of arguments into the a0 a1 a2

96
00:03:50,879 --> 00:03:56,760
but we only saw a z here because we're

97
00:03:53,120 --> 00:03:56,760
calling a function with one ARG

