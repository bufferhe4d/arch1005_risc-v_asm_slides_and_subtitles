1
00:00:00,240 --> 00:00:04,880
so if you stepped through func plus 4

2
00:00:03,159 --> 00:00:06,480
which is not actually the last store but

3
00:00:04,880 --> 00:00:08,960
rather the last manipulation of the

4
00:00:06,480 --> 00:00:11,040
frame pointer this is what you would see

5
00:00:08,960 --> 00:00:14,120
and we have a new thing with a new color

6
00:00:11,040 --> 00:00:16,320
coding here this orangish tone is our

7
00:00:14,120 --> 00:00:19,720
saved return address we actually saw

8
00:00:16,320 --> 00:00:21,439
that main was saving a return address

9
00:00:19,720 --> 00:00:23,400
then we have saved frame pointer

10
00:00:21,439 --> 00:00:26,080
pointing up to 910 and another saved

11
00:00:23,400 --> 00:00:27,920
frame pointer pointing up to a98 where

12
00:00:26,080 --> 00:00:30,160
the frame pointer was first pointing

13
00:00:27,920 --> 00:00:32,000
when we got into this code now one of

14
00:00:30,160 --> 00:00:34,440
the takeaways from this is that it looks

15
00:00:32,000 --> 00:00:36,879
like if a function is calling a sub

16
00:00:34,440 --> 00:00:39,000
routine that is the time when the return

17
00:00:36,879 --> 00:00:41,039
address must be saved and that makes

18
00:00:39,000 --> 00:00:44,280
sense so we start it out we get into

19
00:00:41,039 --> 00:00:46,879
main main has a ra a return address of

20
00:00:44,280 --> 00:00:50,399
how to get back out of main but if main

21
00:00:46,879 --> 00:00:53,000
is going to call into func then the ra

22
00:00:50,399 --> 00:00:55,480
needs to change to get back from func

23
00:00:53,000 --> 00:00:57,199
into main so it needs to save off the

24
00:00:55,480 --> 00:00:59,559
previous return address so it can still

25
00:00:57,199 --> 00:01:01,760
know how to get back out of main after

26
00:00:59,559 --> 00:01:03,480
it gets gets back out of func on the

27
00:01:01,760 --> 00:01:04,960
other hand func doesn't need to do this

28
00:01:03,480 --> 00:01:06,960
func doesn't need to save the return

29
00:01:04,960 --> 00:01:09,479
address it can just leave that in the

30
00:01:06,960 --> 00:01:12,720
return address register and that is why

31
00:01:09,479 --> 00:01:15,240
it's not on the stack so here was from

32
00:01:12,720 --> 00:01:18,200
main we can see this was the store of RA

33
00:01:15,240 --> 00:01:20,880
to stack pointer plus 8 and also at the

34
00:01:18,200 --> 00:01:22,880
very end after it is called func it

35
00:01:20,880 --> 00:01:26,119
returns that loads it back out from

36
00:01:22,880 --> 00:01:28,079
memory into ra we don't see the

37
00:01:26,119 --> 00:01:31,280
equivalent thing inside a func because

38
00:01:28,079 --> 00:01:33,439
it can just use the ra value as is and

39
00:01:31,280 --> 00:01:36,759
implicitly it will be used by that

40
00:01:33,439 --> 00:01:39,520
pseudo instruction r r is going to just

41
00:01:36,759 --> 00:01:42,040
jump back to wherever the ra is set and

42
00:01:39,520 --> 00:01:44,159
we can see that the Ra was set as a

43
00:01:42,040 --> 00:01:47,560
consequence of the jaw instruction the

44
00:01:44,159 --> 00:01:49,719
jaw is like call and it called into func

45
00:01:47,560 --> 00:01:52,399
and it set to the destination register

46
00:01:49,719 --> 00:01:55,040
for the address after jaw this main plus

47
00:01:52,399 --> 00:01:57,600
12 set that to ra so later on with this

48
00:01:55,040 --> 00:01:59,799
R fires off it is going to go back to

49
00:01:57,600 --> 00:02:02,360
this main plus 12 Louis assembly instru

50
00:01:59,799 --> 00:02:04,600
struction so we would notice that

51
00:02:02,360 --> 00:02:07,360
essentially this area of the stack seems

52
00:02:04,600 --> 00:02:10,399
to be used by main both for the saving

53
00:02:07,360 --> 00:02:13,560
of the return address and for the saving

54
00:02:10,399 --> 00:02:15,360
of the frame pointer and this area seems

55
00:02:13,560 --> 00:02:17,560
to be used by func it has that

56
00:02:15,360 --> 00:02:19,959
uninitialized alignment that we've seen

57
00:02:17,560 --> 00:02:23,360
before another takeaway is that it seems

58
00:02:19,959 --> 00:02:26,519
like for leaf functions the saved frame

59
00:02:23,360 --> 00:02:28,480
pointer is at frame pointer minus 8 so

60
00:02:26,519 --> 00:02:30,599
frame pointer is pointing right here and

61
00:02:28,480 --> 00:02:32,760
then minus 8 is going to get us the

62
00:02:30,599 --> 00:02:35,160
saved frame pointer but now if it's a

63
00:02:32,760 --> 00:02:36,920
non-leaf function that has a saved

64
00:02:35,160 --> 00:02:39,239
return address the frame pointer got

65
00:02:36,920 --> 00:02:42,080
pushed down by 8 and so it's actually

66
00:02:39,239 --> 00:02:44,040
frame pointer minus 10 so we would want

67
00:02:42,080 --> 00:02:45,879
to do some more research and see more

68
00:02:44,040 --> 00:02:47,599
functions called in order to see whether

69
00:02:45,879 --> 00:02:49,480
that is a general convention or just

70
00:02:47,599 --> 00:02:52,040
something special happening right here

71
00:02:49,480 --> 00:02:54,120
but we can start to get a sense of maybe

72
00:02:52,040 --> 00:02:56,519
that's what's going on we can also get a

73
00:02:54,120 --> 00:02:58,360
sense of that over allocation where we

74
00:02:56,519 --> 00:03:01,360
thought we solved that mystery in our

75
00:02:58,360 --> 00:03:03,480
mystery listry we thought uninitialized

76
00:03:01,360 --> 00:03:06,319
alignment was just because it needed to

77
00:03:03,480 --> 00:03:08,000
stay on a multiple of hex 10 but now

78
00:03:06,319 --> 00:03:10,280
based on seeing the different ways that

79
00:03:08,000 --> 00:03:12,319
a leaf function behaves from a function

80
00:03:10,280 --> 00:03:15,000
that's going to call another function we

81
00:03:12,319 --> 00:03:17,440
might know that the GCC seems to be

82
00:03:15,000 --> 00:03:19,400
allocating hex6 for both a return

83
00:03:17,440 --> 00:03:21,080
address and a frame pointer and it's

84
00:03:19,400 --> 00:03:23,840
just that it doesn't need to store a

85
00:03:21,080 --> 00:03:25,879
return address for a leaf function again

86
00:03:23,840 --> 00:03:28,439
more data needed we would want to see

87
00:03:25,879 --> 00:03:30,599
more functions called in sequence in

88
00:03:28,439 --> 00:03:32,239
order to see the behavior of the stack

89
00:03:30,599 --> 00:03:34,400
there's another inference game to be

90
00:03:32,239 --> 00:03:36,920
played here then we have the fearful

91
00:03:34,400 --> 00:03:39,879
symmetry observed in main the

92
00:03:36,920 --> 00:03:42,319
allocation of space for the frame by

93
00:03:39,879 --> 00:03:45,959
subtracting 16 and the subsequent

94
00:03:42,319 --> 00:03:48,040
deallocation by adding 16 the storage of

95
00:03:45,959 --> 00:03:50,360
the return address and the frame pointer

96
00:03:48,040 --> 00:03:52,159
followed by the loading the stored

97
00:03:50,360 --> 00:03:53,720
return address and frame pointer but

98
00:03:52,159 --> 00:03:55,200
there's something that bugs me about

99
00:03:53,720 --> 00:03:57,239
this and if it bugs me it's definitely

100
00:03:55,200 --> 00:03:59,120
going to bug Thanos and that's the fact

101
00:03:57,239 --> 00:04:01,360
that while it is logically balanced it

102
00:03:59,120 --> 00:04:04,120
is not per perfectly reverse order

103
00:04:01,360 --> 00:04:06,799
symmetrical I expect symmetry across the

104
00:04:04,120 --> 00:04:11,360
horizontal axis and so although we have

105
00:04:06,799 --> 00:04:13,560
ADDI -6 add i+ 16 we have Store return

106
00:04:11,360 --> 00:04:15,920
address store frame pointer and I would

107
00:04:13,560 --> 00:04:17,959
want to see load frame pointer load

108
00:04:15,920 --> 00:04:19,440
return address so unfortunately this is

109
00:04:17,959 --> 00:04:21,040
just the kind of thing that bothers

110
00:04:19,440 --> 00:04:23,479
those of us who care about perfect

111
00:04:21,040 --> 00:04:26,360
symmetry anyways let's throw some chaos

112
00:04:23,479 --> 00:04:28,040
magic at this code oh and we landed in a

113
00:04:26,360 --> 00:04:31,400
completely different reality where

114
00:04:28,040 --> 00:04:34,280
mutants rule the world no so we landed

115
00:04:31,400 --> 00:04:36,360
at compiler Explorer God bolt. org

116
00:04:34,280 --> 00:04:38,479
that's actually the person's last name

117
00:04:36,360 --> 00:04:41,080
not just saying it's like a lightning

118
00:04:38,479 --> 00:04:43,160
bolt from God anyways God bolt. org has

119
00:04:41,080 --> 00:04:46,039
compiler Explorer which is a very cool

120
00:04:43,160 --> 00:04:48,639
tool where you can ask it to take some

121
00:04:46,039 --> 00:04:51,199
source code and show you the disassembly

122
00:04:48,639 --> 00:04:53,120
for different versions of compilers so

123
00:04:51,199 --> 00:04:55,199
you can explore how different compilers

124
00:04:53,120 --> 00:04:57,560
would compile a given piece of code but

125
00:04:55,199 --> 00:05:00,199
if we put in this function and we look

126
00:04:57,560 --> 00:05:02,880
at func we see that actually it's saying

127
00:05:00,199 --> 00:05:05,840
that it's not jaing to func it's calling

128
00:05:02,880 --> 00:05:07,800
to func so what's up with that well we

129
00:05:05,840 --> 00:05:10,000
first just to you know show exactly what

130
00:05:07,800 --> 00:05:12,600
we do and to show what's going on behind

131
00:05:10,000 --> 00:05:14,440
the scenes going back here for a second

132
00:05:12,600 --> 00:05:16,440
this is the default output that you

133
00:05:14,440 --> 00:05:18,560
would see but I want to see you know I

134
00:05:16,440 --> 00:05:20,080
think there's pseudo instructions about

135
00:05:18,560 --> 00:05:22,479
I want to see what's going on behind the

136
00:05:20,080 --> 00:05:24,800
call so to do that I make sure that I'm

137
00:05:22,479 --> 00:05:28,240
putting in the exact same version of GCC

138
00:05:24,800 --> 00:05:30,440
that we have in our qmu VM I'm also

139
00:05:28,240 --> 00:05:32,319
saying to compile as a binary object

140
00:05:30,440 --> 00:05:34,639
this is the key thing only if you say

141
00:05:32,319 --> 00:05:37,120
compile as binary object so that then it

142
00:05:34,639 --> 00:05:38,960
decompiles it or disassembles it really

143
00:05:37,120 --> 00:05:40,600
only then will you actually get to see

144
00:05:38,960 --> 00:05:42,120
all of the instructions behind the

145
00:05:40,600 --> 00:05:43,960
scenes and also you want to click off

146
00:05:42,120 --> 00:05:46,800
that you're not using Intel syntax

147
00:05:43,960 --> 00:05:49,520
because in this class we use a new

148
00:05:46,800 --> 00:05:51,680
syntax and the key thing is that now

149
00:05:49,520 --> 00:05:54,160
under those options it's showing us that

150
00:05:51,680 --> 00:05:58,360
the call is not even a jaw behind the

151
00:05:54,160 --> 00:06:00,160
scenes it's actually a we PC plus a Jer

152
00:05:58,360 --> 00:06:02,080
and this is a little bit weird because

153
00:06:00,160 --> 00:06:04,560
for some reason it's generating

154
00:06:02,080 --> 00:06:06,599
different assembly under the assumption

155
00:06:04,560 --> 00:06:08,400
of the exact same compilers we're using

156
00:06:06,599 --> 00:06:10,880
so not really sure what's up with that

157
00:06:08,400 --> 00:06:12,639
but what I can tell you is that call is

158
00:06:10,880 --> 00:06:15,000
one of those Insidious pseudo

159
00:06:12,639 --> 00:06:17,960
instructions for instance in that case

160
00:06:15,000 --> 00:06:22,759
when generated by God bolt. org compiler

161
00:06:17,960 --> 00:06:24,960
Explorer call is actually oepc plus Jer

162
00:06:22,759 --> 00:06:27,000
so you can't trust pseudo instructions

163
00:06:24,960 --> 00:06:28,720
they could be two assembly instructions

164
00:06:27,000 --> 00:06:31,120
in address pretending to be one

165
00:06:28,720 --> 00:06:33,440
instruction but the call is just one of

166
00:06:31,120 --> 00:06:35,840
many in a lounge of pseudo instructions

167
00:06:33,440 --> 00:06:38,759
a lounge being a group of lizards we can

168
00:06:35,840 --> 00:06:41,199
see from the documentation that it lists

169
00:06:38,759 --> 00:06:44,400
many things there's jaw Jr we saw that

170
00:06:41,199 --> 00:06:48,360
before joller Rett and call so call it

171
00:06:44,400 --> 00:06:50,039
says yes is a wepc plus J joller but

172
00:06:48,360 --> 00:06:51,599
other things we've seen before is just

173
00:06:50,039 --> 00:06:53,199
that it's calling it a pseudo

174
00:06:51,599 --> 00:06:55,960
instruction if it shows you something

175
00:06:53,199 --> 00:06:58,919
like Jaw and only an offset because it's

176
00:06:55,960 --> 00:07:01,319
saying implicitly the jaw is using x1

177
00:06:58,919 --> 00:07:03,960
which is the return address register so

178
00:07:01,319 --> 00:07:06,120
it's JW x1 and offset so the

179
00:07:03,960 --> 00:07:07,800
disassembler may show it like that but

180
00:07:06,120 --> 00:07:09,960
it is not something that doesn't take

181
00:07:07,800 --> 00:07:11,879
two arguments it does take two arguments

182
00:07:09,960 --> 00:07:14,280
that's just the pseudo instruction form

183
00:07:11,879 --> 00:07:16,639
so we saw jel just a second ago we saw

184
00:07:14,280 --> 00:07:19,639
Jr very early in the class specifically

185
00:07:16,639 --> 00:07:23,240
compressed Jr we said compressed Jr is

186
00:07:19,639 --> 00:07:26,680
JR Jr is jll it is specifically a joller

187
00:07:23,240 --> 00:07:28,759
with a destination register for its link

188
00:07:26,680 --> 00:07:31,720
of x0 which means just throw away the

189
00:07:28,759 --> 00:07:34,479
linkage it's just going to do a one-way

190
00:07:31,720 --> 00:07:36,440
jump with no hope of returning because

191
00:07:34,479 --> 00:07:39,560
actually that is where we saw it we saw

192
00:07:36,440 --> 00:07:41,919
a r was a compressed Jr was a Jr was a

193
00:07:39,560 --> 00:07:44,759
jwar no hope of returning because you

194
00:07:41,919 --> 00:07:47,199
store the link register into x0 which

195
00:07:44,759 --> 00:07:49,199
remains zero and there's an offset but

196
00:07:47,199 --> 00:07:50,599
it's an offset of zero and that's

197
00:07:49,199 --> 00:07:53,360
exactly what they're telling you down

198
00:07:50,599 --> 00:07:56,159
here R is that jwar and then again

199
00:07:53,360 --> 00:07:58,159
joller without showing two parameters

200
00:07:56,159 --> 00:08:00,039
well it's actually using two parameters

201
00:07:58,159 --> 00:08:02,280
it's just one of them is a zero register

202
00:08:00,039 --> 00:08:04,199
and one of them is a zero immediate and

203
00:08:02,280 --> 00:08:05,879
I've hidden a spoiler for later in the

204
00:08:04,199 --> 00:08:08,319
class of another pseudo instruction that

205
00:08:05,879 --> 00:08:10,080
we will encounter soon so I'll just

206
00:08:08,319 --> 00:08:11,400
point out that although the spec is

207
00:08:10,080 --> 00:08:13,599
listing these various pseudo

208
00:08:11,400 --> 00:08:17,400
instructions the call that it's showing

209
00:08:13,599 --> 00:08:20,080
with wepc and Jer is a far call so it's

210
00:08:17,400 --> 00:08:22,639
calling to some Far Away location you

211
00:08:20,080 --> 00:08:24,840
can also have the notion of a near call

212
00:08:22,639 --> 00:08:26,879
which would be PC relative which is

213
00:08:24,840 --> 00:08:28,960
where exactly what we're seeing in our

214
00:08:26,879 --> 00:08:31,520
actual code that is where a call is

215
00:08:28,960 --> 00:08:34,399
actually backed by a jaw and I already

216
00:08:31,520 --> 00:08:36,360
pointed it out but it just calls these

217
00:08:34,399 --> 00:08:37,599
simplified representation pseudo

218
00:08:36,360 --> 00:08:39,959
instructions as

219
00:08:37,599 --> 00:08:43,120
well okay what did we pick up in this

220
00:08:39,959 --> 00:08:45,839
example we picked up jaw and looking at

221
00:08:43,120 --> 00:08:48,360
this we've got jaw sitting here side by

222
00:08:45,839 --> 00:08:53,120
side with Jer but then we have call

223
00:08:48,360 --> 00:08:53,120
which was a wepc and a Jer

