1
00:00:00,560 --> 00:00:05,240
so what did you see hopefully you saw

2
00:00:02,639 --> 00:00:06,799
something like this your frame pointer

3
00:00:05,240 --> 00:00:09,160
once again pointing at where the

4
00:00:06,799 --> 00:00:11,599
previous frame pointer pointed but now

5
00:00:09,160 --> 00:00:13,759
for our local variables 2 64-bit

6
00:00:11,599 --> 00:00:15,599
variables we don't have any of that

7
00:00:13,759 --> 00:00:17,800
interesting padding down at the bottom

8
00:00:15,599 --> 00:00:21,160
we have affected odd balls here and

9
00:00:17,800 --> 00:00:23,320
leafless bold face here no extra space

10
00:00:21,160 --> 00:00:25,640
so we've got the extra space up top but

11
00:00:23,320 --> 00:00:28,039
no extra space down bottom so let's go

12
00:00:25,640 --> 00:00:30,800
ahead and compare directly our two ins

13
00:00:28,039 --> 00:00:32,559
versus two long Longs and we we see is

14
00:00:30,800 --> 00:00:34,640
well their Stacks start at slightly

15
00:00:32,559 --> 00:00:36,920
different locations and I'll probably

16
00:00:34,640 --> 00:00:39,040
add a little video about that later on

17
00:00:36,920 --> 00:00:41,120
where we talk about why these Stacks are

18
00:00:39,040 --> 00:00:42,800
changing between different things and

19
00:00:41,120 --> 00:00:44,520
and spoiler alert the answer is

20
00:00:42,800 --> 00:00:47,840
basically because of the length of their

21
00:00:44,520 --> 00:00:51,120
names but here we have the Obsolete and

22
00:00:47,840 --> 00:00:53,280
scalable which filled up this 64 bits

23
00:00:51,120 --> 00:00:55,840
but when we move up to 64-bit size

24
00:00:53,280 --> 00:00:57,920
things we have the 64 bits and that 64

25
00:00:55,840 --> 00:01:00,199
bits so we've got that uninitialized

26
00:00:57,920 --> 00:01:02,719
data after the saved frame pointer but

27
00:01:00,199 --> 00:01:05,400
otherwise if we take up all the space

28
00:01:02,719 --> 00:01:07,280
then this uninitialize down at the

29
00:01:05,400 --> 00:01:10,360
lowest addresses of the stack but really

30
00:01:07,280 --> 00:01:12,920
the stack top is actually filled in so

31
00:01:10,360 --> 00:01:15,439
the takeaway from two long Longs is that

32
00:01:12,920 --> 00:01:18,520
the compiler stores those two 64-bit

33
00:01:15,439 --> 00:01:20,680
constants at data past the end of this

34
00:01:18,520 --> 00:01:23,479
assembly and you could confirm that if

35
00:01:20,680 --> 00:01:26,520
you used your uh GDB debugger you could

36
00:01:23,479 --> 00:01:28,840
just go ahead and examine as heximal

37
00:01:26,520 --> 00:01:30,799
giant words and you would have seen the

38
00:01:28,840 --> 00:01:32,079
leafless bolt phase and affected odd

39
00:01:30,799 --> 00:01:34,479
walls you would have figured out which

40
00:01:32,079 --> 00:01:37,079
one is which and consequently which one

41
00:01:34,479 --> 00:01:40,159
is being stored to which location on the

42
00:01:37,079 --> 00:01:44,200
stack here it's frame pointer minus 32

43
00:01:40,159 --> 00:01:45,960
and here it is frame pointer minus 24 so

44
00:01:44,200 --> 00:01:48,719
basically the net result of all this

45
00:01:45,960 --> 00:01:51,000
stuff that I'm sharing with you here is

46
00:01:48,719 --> 00:01:54,360
loading up the data that is stored

47
00:01:51,000 --> 00:01:57,159
afterwards at these 660 and 668 another

48
00:01:54,360 --> 00:01:59,039
takeaway is that unoptimized GCC code

49
00:01:57,159 --> 00:02:01,000
seems to allocate local variables in

50
00:01:59,039 --> 00:02:03,119
order of Declaration a starting from

51
00:02:01,000 --> 00:02:05,240
lower addresses to higher so we saw sort

52
00:02:03,119 --> 00:02:07,880
of that things were backwards and that

53
00:02:05,240 --> 00:02:09,959
the scalable moved down and the obsolete

54
00:02:07,880 --> 00:02:13,599
went above it same thing happened with

55
00:02:09,959 --> 00:02:16,239
the 64-bit variables leafless bold face

56
00:02:13,599 --> 00:02:18,440
and affected oddballs we can't yet say

57
00:02:16,239 --> 00:02:20,160
anything about whether optimized GCC

58
00:02:18,440 --> 00:02:22,280
would behave the same way but we can say

59
00:02:20,160 --> 00:02:24,440
for unoptimized that seems to be what's

60
00:02:22,280 --> 00:02:27,040
happening right and here's the code so

61
00:02:24,440 --> 00:02:29,519
scalable went down and then obsolete

62
00:02:27,040 --> 00:02:32,000
went at a higher address leafless bold

63
00:02:29,519 --> 00:02:34,280
face went down and affected oddballs

64
00:02:32,000 --> 00:02:36,879
went at a higher address now before I

65
00:02:34,280 --> 00:02:38,920
mark down wepc on our diagrams let's go

66
00:02:36,879 --> 00:02:42,480
ahead and invoke the chaos magic and say

67
00:02:38,920 --> 00:02:44,800
no more aliases blow it away and we've

68
00:02:42,480 --> 00:02:48,120
got one little new thing here we see

69
00:02:44,800 --> 00:02:50,720
that actually this load dword can have a

70
00:02:48,120 --> 00:02:52,800
compressed form so we see compressed

71
00:02:50,720 --> 00:02:54,560
load dword multiple places we don't

72
00:02:52,800 --> 00:02:56,920
actually see a compressed store dword

73
00:02:54,560 --> 00:02:58,760
and we still actually see it using a

74
00:02:56,920 --> 00:03:01,840
non-compressed load dword for some

75
00:02:58,760 --> 00:03:04,120
reason so basically we've got compressed

76
00:03:01,840 --> 00:03:06,959
load dword new compressed assembly

77
00:03:04,120 --> 00:03:09,239
instruction now as a reminder here is

78
00:03:06,959 --> 00:03:11,040
how the load dword worked it had a

79
00:03:09,239 --> 00:03:14,040
12-bit immediate it had a source

80
00:03:11,040 --> 00:03:15,959
register and it operated like normal

81
00:03:14,040 --> 00:03:17,920
instructions and that you're loading

82
00:03:15,959 --> 00:03:21,480
from this side from the right to the

83
00:03:17,920 --> 00:03:23,879
left so rs1 plus the sign extended

84
00:03:21,480 --> 00:03:26,159
immediate 12 pull from memory

85
00:03:23,879 --> 00:03:29,120
parentheses pull from memory and store

86
00:03:26,159 --> 00:03:31,519
into Rd so how does compressed d load

87
00:03:29,120 --> 00:03:33,480
dword differ from that so compressed

88
00:03:31,519 --> 00:03:35,840
load double word doesn't have enough

89
00:03:33,480 --> 00:03:37,720
space in that 16bit encoding for a 12-

90
00:03:35,840 --> 00:03:41,599
bit immediate so it uses an 8bit

91
00:03:37,720 --> 00:03:43,799
immediate and so we have the rs1 prime

92
00:03:41,599 --> 00:03:47,720
which is restricted to only the

93
00:03:43,799 --> 00:03:50,760
registers X8 through X15 so rs1 prime

94
00:03:47,720 --> 00:03:52,200
plus the unsigned immediate 8 unsigned

95
00:03:50,760 --> 00:03:54,560
meaning that it's going to be zero

96
00:03:52,200 --> 00:03:56,439
extended not sign extended take those

97
00:03:54,560 --> 00:03:59,480
two things add them together treat it as

98
00:03:56,439 --> 00:04:01,560
a dword memory address and pluck a dword

99
00:03:59,480 --> 00:04:03,799
out out of memory and store into Rd

100
00:04:01,560 --> 00:04:06,879
prime which again is restricted to only

101
00:04:03,799 --> 00:04:09,879
being X8 through X15 so this unsigned

102
00:04:06,879 --> 00:04:11,840
8bit immediate is scaled by eight and

103
00:04:09,879 --> 00:04:13,360
that means that the lower three bits are

104
00:04:11,840 --> 00:04:16,359
going to be implicitly set to zero so

105
00:04:13,360 --> 00:04:19,040
only multiples of eight 0 8 16 and so

106
00:04:16,359 --> 00:04:21,040
forth and again this is a load so it

107
00:04:19,040 --> 00:04:22,680
reads from right to left like a normal

108
00:04:21,040 --> 00:04:24,800
assembly instruction calculate the stuff

109
00:04:22,680 --> 00:04:27,759
on the right move it to the left and

110
00:04:24,800 --> 00:04:29,720
again parentheses for loads and stores

111
00:04:27,759 --> 00:04:30,600
means that you're using something as the

112
00:04:29,720 --> 00:04:34,039
memory

113
00:04:30,600 --> 00:04:36,320
address so in this section we picked up

114
00:04:34,039 --> 00:04:39,600
we

115
00:04:36,320 --> 00:04:42,160
PCC and we also picked up a compressed

116
00:04:39,600 --> 00:04:44,280
instruction so here's our wepc next to

117
00:04:42,160 --> 00:04:46,240
LUI we saw LUI was the first

118
00:04:44,280 --> 00:04:48,759
assembly instruction that could take and

119
00:04:46,240 --> 00:04:52,440
load an upper immediate so that loaded

120
00:04:48,759 --> 00:04:56,280
upper 20 Bits And wepc as well loads

121
00:04:52,440 --> 00:04:59,680
plus uh an upper 20 bits to the PC and

122
00:04:56,280 --> 00:05:01,720
then it stores it into a register and

123
00:04:59,680 --> 00:05:04,199
also we picked up the compressed load

124
00:05:01,720 --> 00:05:06,720
the generic form as opposed to the stack

125
00:05:04,199 --> 00:05:08,600
pointer specific form but while this is

126
00:05:06,720 --> 00:05:10,800
what we know we are not done yet we have

127
00:05:08,600 --> 00:05:14,960
not actually solved the mystery so let's

128
00:05:10,800 --> 00:05:14,960
continue playing the inference game

