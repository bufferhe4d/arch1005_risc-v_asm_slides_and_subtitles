1
00:00:00,080 --> 00:00:04,040
so the takeaways from this example is

2
00:00:02,080 --> 00:00:05,520
that I played the game with taking an

3
00:00:04,040 --> 00:00:09,840
input via

4
00:00:05,520 --> 00:00:12,759
a2l asky string to long with O2

5
00:00:09,840 --> 00:00:15,639
optimization because I saw that at o00

6
00:00:12,759 --> 00:00:17,960
optimization GCC really liked to create

7
00:00:15,639 --> 00:00:20,480
those branch if greater than or equal

8
00:00:17,960 --> 00:00:23,279
with unsigned rather than branch if less

9
00:00:20,480 --> 00:00:26,359
than with unsigned so basically this was

10
00:00:23,279 --> 00:00:28,720
just a game to trick GCC in some sense

11
00:00:26,359 --> 00:00:30,039
into generating exactly what I wanted

12
00:00:28,720 --> 00:00:32,119
the other thing that's a little bit

13
00:00:30,039 --> 00:00:35,800
interesting and weird about this code is

14
00:00:32,119 --> 00:00:37,800
that it put the return three case after

15
00:00:35,800 --> 00:00:40,120
the R so it didn't like put it in line

16
00:00:37,800 --> 00:00:43,120
or anything it said okay there's going

17
00:00:40,120 --> 00:00:45,719
to be something like branch if less than

18
00:00:43,120 --> 00:00:49,840
unsigned if that's true then jump down

19
00:00:45,719 --> 00:00:52,440
here to main plus 40 load the three into

20
00:00:49,840 --> 00:00:55,640
a z or return register and then jump

21
00:00:52,440 --> 00:00:57,600
backwards to main plus 34 and exit out

22
00:00:55,640 --> 00:00:59,239
of the function so that was kind of

23
00:00:57,600 --> 00:01:01,079
interesting I don't know why it did it

24
00:00:59,239 --> 00:01:03,680
that way but I found that to be a little

25
00:01:01,079 --> 00:01:06,159
bit interesting picked up a new assembly

26
00:01:03,680 --> 00:01:09,479
instruction branch if less than and

27
00:01:06,159 --> 00:01:12,520
branch if less than unsigned here we go

28
00:01:09,479 --> 00:01:14,720
filling things in looking good looking

29
00:01:12,520 --> 00:01:16,400
pretty full but I could stand to learn a

30
00:01:14,720 --> 00:01:19,080
few more branch instructions couldn't

31
00:01:16,400 --> 00:01:19,080
you

