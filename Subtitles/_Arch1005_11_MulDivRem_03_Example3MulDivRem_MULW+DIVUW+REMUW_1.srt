1
00:00:00,199 --> 00:00:04,520
now in this class you've already seen

2
00:00:02,480 --> 00:00:06,160
the core things that you need to know

3
00:00:04,520 --> 00:00:08,719
about multiplication division and

4
00:00:06,160 --> 00:00:11,599
remainers there's multiply keep the low

5
00:00:08,719 --> 00:00:13,679
multiply keep the high and Division and

6
00:00:11,599 --> 00:00:15,759
remainder so now it's just variations on

7
00:00:13,679 --> 00:00:19,359
a theme and that theme for example three

8
00:00:15,759 --> 00:00:24,000
multiply div REM is integers we now have

9
00:00:19,359 --> 00:00:25,680
integer a times divide and modulus so

10
00:00:24,000 --> 00:00:29,039
what do we usually see when we have

11
00:00:25,680 --> 00:00:33,960
64-bit code that uses 32-bit values

12
00:00:29,039 --> 00:00:37,680
that's right W suffixed instructions m w

13
00:00:33,960 --> 00:00:39,160
dww and remu W so exact same assembly

14
00:00:37,680 --> 00:00:42,520
instructions as we saw in our first

15
00:00:39,160 --> 00:00:45,559
example now just W suffixed so w

16
00:00:42,520 --> 00:00:47,920
suffixed is word sized so we have

17
00:00:45,559 --> 00:00:51,440
multiply treating them as word sized

18
00:00:47,920 --> 00:00:54,039
registers and keep the low word size so

19
00:00:51,440 --> 00:00:56,600
rs1 is our source this is a

20
00:00:54,039 --> 00:01:00,760
64bit uh extension so this is only in

21
00:00:56,600 --> 00:01:03,360
the RV 64m extension so RS one treat it

22
00:01:00,760 --> 00:01:06,600
as a 32-bit value to so take the lower

23
00:01:03,360 --> 00:01:09,439
32bits multiply it by the low 32bits of

24
00:01:06,600 --> 00:01:12,439
rs2 take the result of that which is

25
00:01:09,439 --> 00:01:14,680
keep only the low 32bits of the 64-bit

26
00:01:12,439 --> 00:01:17,400
result and then sign extended and put it

27
00:01:14,680 --> 00:01:19,280
in a 64-bit register so all the same

28
00:01:17,400 --> 00:01:21,880
you're just operating on word size

29
00:01:19,280 --> 00:01:28,000
registers all the same as a normal mle

30
00:01:21,880 --> 00:01:30,680
so t0 and T1 we've got 2 * all A's but

31
00:01:28,000 --> 00:01:34,880
we're only going to keep the bottom

32
00:01:30,680 --> 00:01:37,399
um 32 bits so the bottom 32bits time 2

33
00:01:34,880 --> 00:01:39,799
is going to overflow the possible 32bit

34
00:01:37,399 --> 00:01:42,759
value but then it's just truncated we

35
00:01:39,799 --> 00:01:45,719
keep the bottom 32-bit result and then

36
00:01:42,759 --> 00:01:50,560
sign extended up to 64 bits another

37
00:01:45,719 --> 00:01:53,360
example m w we've got 2 * 7 and all

38
00:01:50,560 --> 00:01:56,880
zeros and the net result of that is

39
00:01:53,360 --> 00:01:59,680
going to be e and all zeros and then

40
00:01:56,880 --> 00:02:01,799
sign extended up to become a 64-bit

41
00:01:59,680 --> 00:02:04,439
value value all right then the divide

42
00:02:01,799 --> 00:02:06,840
unsign that we saw before but treat them

43
00:02:04,439 --> 00:02:11,039
as word size registers cuz this is an RV

44
00:02:06,840 --> 00:02:14,560
64m instruction so bottom 32 bits of rs1

45
00:02:11,039 --> 00:02:17,480
divided by bottom 32 bits of rs2 then

46
00:02:14,560 --> 00:02:22,560
you have the result as a 32-bit value

47
00:02:17,480 --> 00:02:25,760
that is s extended so 5 / 4 well now

48
00:02:22,560 --> 00:02:29,200
instead of 5 divid 6 5 / 4 four goes in

49
00:02:25,760 --> 00:02:32,000
one time nothing special about that then

50
00:02:29,200 --> 00:02:35,519
we've got 5 divided by six as before and

51
00:02:32,000 --> 00:02:38,000
that goes in zero times not exactly the

52
00:02:35,519 --> 00:02:40,400
most creative example I know remainder

53
00:02:38,000 --> 00:02:44,159
same thing remainder unsigned word size

54
00:02:40,400 --> 00:02:45,760
registers so lower 32 modulo lower 32

55
00:02:44,159 --> 00:02:47,720
and it's only going to be a 32-bit

56
00:02:45,760 --> 00:02:51,599
result but just putting it there just in

57
00:02:47,720 --> 00:02:55,239
case and sign extend the result so B

58
00:02:51,599 --> 00:02:58,200
modulus 3 is a remainder of

59
00:02:55,239 --> 00:03:00,640
two on the other hand a five modulus six

60
00:02:58,200 --> 00:03:03,159
is a remainder of five because six goes

61
00:03:00,640 --> 00:03:05,480
into five zero times with a five

62
00:03:03,159 --> 00:03:07,280
remainder okay so like I said it's all

63
00:03:05,480 --> 00:03:09,360
just permutations from this point in

64
00:03:07,280 --> 00:03:11,239
multiplication and division and

65
00:03:09,360 --> 00:03:12,959
remainders so stop step through the

66
00:03:11,239 --> 00:03:15,040
assembly check your understanding that

67
00:03:12,959 --> 00:03:19,480
you understand that it's using 32-bit

68
00:03:15,040 --> 00:03:19,480
register values in 64-bit code

