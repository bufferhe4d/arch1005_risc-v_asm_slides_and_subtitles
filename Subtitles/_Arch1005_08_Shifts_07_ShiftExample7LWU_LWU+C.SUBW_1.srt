1
00:00:00,120 --> 00:00:07,160
in shift example 7 LW we have a unsigned

2
00:00:03,800 --> 00:00:09,840
int a set to one one unsigned int B set

3
00:00:07,160 --> 00:00:11,719
to x22 and we take an argument on the

4
00:00:09,840 --> 00:00:14,200
command line put it into the signed

5
00:00:11,719 --> 00:00:17,560
integer C and we're going to do B

6
00:00:14,200 --> 00:00:21,400
shifted left by C and then minus equal

7
00:00:17,560 --> 00:00:23,119
that value into a so here is the code

8
00:00:21,400 --> 00:00:25,840
that that generates and there's no new

9
00:00:23,119 --> 00:00:27,519
assembly instructions and these are not

10
00:00:25,840 --> 00:00:29,119
the instructions you're looking for at

11
00:00:27,519 --> 00:00:31,359
least they're not the instructions I'm

12
00:00:29,119 --> 00:00:34,640
looking for because at this point in the

13
00:00:31,359 --> 00:00:37,079
class I am trying to hammer out the last

14
00:00:34,640 --> 00:00:39,640
assembly instructions that we're missing

15
00:00:37,079 --> 00:00:42,719
and finish off our checklist so what I'm

16
00:00:39,640 --> 00:00:45,440
looking for right now is lwo and I was

17
00:00:42,719 --> 00:00:48,480
not able to find lwo until I played this

18
00:00:45,440 --> 00:00:50,760
little game with shifts so specifically

19
00:00:48,480 --> 00:00:53,760
I had to play the game and use clay

20
00:00:50,760 --> 00:00:58,440
because GCC would never create this lwo

21
00:00:53,760 --> 00:01:01,320
for me but claying would so this code

22
00:00:58,440 --> 00:01:04,320
will generate a new assembly instruction

23
00:01:01,320 --> 00:01:07,040
lwu and we're going to see that now that

24
00:01:04,320 --> 00:01:09,479
is load Word unsigned so we've already

25
00:01:07,040 --> 00:01:12,560
actually seen load Word long long time

26
00:01:09,479 --> 00:01:15,240
ago remember a word is a 32-bit value

27
00:01:12,560 --> 00:01:18,360
but load Word unsigned is going to say

28
00:01:15,240 --> 00:01:21,840
don't sign extended just zero extended

29
00:01:18,360 --> 00:01:24,040
so load Word unsigned we have a source

30
00:01:21,840 --> 00:01:26,320
address so this is a load it's coming

31
00:01:24,040 --> 00:01:29,119
from memory source address plus sign

32
00:01:26,320 --> 00:01:30,439
extended immediate 12 and take that as a

33
00:01:29,119 --> 00:01:32,360
memory address

34
00:01:30,439 --> 00:01:35,159
and then here because it's unsigned

35
00:01:32,360 --> 00:01:37,799
we're going to zero extend the 32-bit

36
00:01:35,159 --> 00:01:40,880
value into the 64-bit register recall

37
00:01:37,799 --> 00:01:44,960
that LW was one of those W suffix things

38
00:01:40,880 --> 00:01:47,119
that is a 6 RV64I assembly instruction

39
00:01:44,960 --> 00:01:49,240
so I could tell you the repeated things

40
00:01:47,119 --> 00:01:51,680
that I always tell you like this one

41
00:01:49,240 --> 00:01:54,119
reads from right to left so memory over

42
00:01:51,680 --> 00:01:55,920
here into register or the fact that the

43
00:01:54,119 --> 00:01:58,320
parentheses means that you're reading

44
00:01:55,920 --> 00:02:01,039
from memory but I won't do that I won't

45
00:01:58,320 --> 00:02:03,759
bore you with that copy and paste repeat

46
00:02:01,039 --> 00:02:06,240
from earlier in the class but I will

47
00:02:03,759 --> 00:02:08,280
tell you it is now time for you to step

48
00:02:06,240 --> 00:02:11,800
through the assembly and make sure you

49
00:02:08,280 --> 00:02:11,800
understand what's going on

