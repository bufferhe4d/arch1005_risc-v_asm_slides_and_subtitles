1
00:00:00,080 --> 00:00:03,600
so at this point knowing that all we're

2
00:00:01,560 --> 00:00:06,040
doing is collecting the remainder

3
00:00:03,600 --> 00:00:07,759
instruction variants for example for

4
00:00:06,040 --> 00:00:10,840
divm we're going to collect the

5
00:00:07,759 --> 00:00:14,120
remainder of the divisions and remainers

6
00:00:10,840 --> 00:00:15,799
so we play the game as we usually do to

7
00:00:14,120 --> 00:00:18,000
hide the values that are going to be

8
00:00:15,799 --> 00:00:19,920
used from the compiler and we're also

9
00:00:18,000 --> 00:00:22,359
going to play the size game by using

10
00:00:19,920 --> 00:00:24,680
long Longs for some variables and an INT

11
00:00:22,359 --> 00:00:27,840
for other variables so we get the W

12
00:00:24,680 --> 00:00:29,560
variant so compile this code and you get

13
00:00:27,840 --> 00:00:30,480
this assembly for new assembly

14
00:00:29,560 --> 00:00:35,320
instruction

15
00:00:30,480 --> 00:00:37,840
div div W REM and remw div is the signed

16
00:00:35,320 --> 00:00:40,520
divide divide assuming that the operands

17
00:00:37,840 --> 00:00:43,840
are assed and it divides the X length

18
00:00:40,520 --> 00:00:46,440
rs1 divided X length rs2 so again it

19
00:00:43,840 --> 00:00:49,199
could be 32 could be 64 bit treat them

20
00:00:46,440 --> 00:00:50,719
as signed round towards zero as usual

21
00:00:49,199 --> 00:00:54,559
right we just say how many times does

22
00:00:50,719 --> 00:00:57,160
rs2 go into rs1 evenly not creating a

23
00:00:54,559 --> 00:00:59,239
floating point decimal version so round

24
00:00:57,160 --> 00:01:01,519
it towards zero and store the result

25
00:00:59,239 --> 00:01:03,600
into the destination register so if we

26
00:01:01,519 --> 00:01:09,479
did a div on a

27
00:01:03,600 --> 00:01:12,920
signed 5534 in heximal or FS a a c

28
00:01:09,479 --> 00:01:16,479
divide that by six and we get this all

29
00:01:12,920 --> 00:01:19,680
FS 1 C D so hex

30
00:01:16,479 --> 00:01:23,960
e33 e33 you can of course check this

31
00:01:19,680 --> 00:01:27,640
with your own calculator divide -9 by

32
00:01:23,960 --> 00:01:31,159
three and of course you should get -3 so

33
00:01:27,640 --> 00:01:33,560
on to divide word size which is again

34
00:01:31,159 --> 00:01:35,680
same thing treat it as signed and use

35
00:01:33,560 --> 00:01:38,560
the word size of the register so take

36
00:01:35,680 --> 00:01:40,640
the lower 32 bits of rs1 and treat that

37
00:01:38,560 --> 00:01:42,240
as signed and you can see how this

38
00:01:40,640 --> 00:01:44,439
distinction of sign versus unsign

39
00:01:42,240 --> 00:01:46,880
becomes important here because when you

40
00:01:44,439 --> 00:01:50,040
truncate down the 64bit register to only

41
00:01:46,880 --> 00:01:52,759
32 bits now it's going to be is the sign

42
00:01:50,040 --> 00:01:56,240
bit of the most significant bit of the

43
00:01:52,759 --> 00:01:57,439
32 bit value is that set and that Tre

44
00:01:56,240 --> 00:01:59,560
determines whether or not it's being

45
00:01:57,439 --> 00:02:02,240
treated as a negative or positive number

46
00:01:59,560 --> 00:02:04,840
so take the lower 32 treat it as signed

47
00:02:02,240 --> 00:02:08,440
divide by the lower 32 of rs2

48
00:02:04,840 --> 00:02:14,120
interpreted as signed so going back to

49
00:02:08,440 --> 00:02:17,400
this example of 5534 or all FS a a C c

50
00:02:14,120 --> 00:02:20,239
divide by 6 we are now going to be using

51
00:02:17,400 --> 00:02:22,319
it as a word signed value word sized

52
00:02:20,239 --> 00:02:25,640
value rather so we're only taking the

53
00:02:22,319 --> 00:02:29,200
bottom 32 bits but even as the bottom 32

54
00:02:25,640 --> 00:02:32,959
bits this is still - 5534 and therefore

55
00:02:29,200 --> 00:02:34,840
once again the result will be one CD

56
00:02:32,959 --> 00:02:39,400
with all Fs in the upper bits it would

57
00:02:34,840 --> 00:02:41,920
just be the 32-bit FFF F FF 1 CD but we

58
00:02:39,400 --> 00:02:45,120
then subsequently do the sign extension

59
00:02:41,920 --> 00:02:50,200
up to the 64-bit value another example

60
00:02:45,120 --> 00:02:53,599
div W or div yep div w 5 / 6 my old

61
00:02:50,200 --> 00:02:55,959
stall word goes in there zero times

62
00:02:53,599 --> 00:02:59,200
remainder as signed values so treat the

63
00:02:55,959 --> 00:03:01,599
rs1 and rs2 as signed do the modulo or

64
00:02:59,200 --> 00:03:03,280
remainder op operation and see what you

65
00:03:01,599 --> 00:03:05,760
come out with in the destination

66
00:03:03,280 --> 00:03:07,760
register so now we're going to do that

67
00:03:05,760 --> 00:03:11,959
negative hex

68
00:03:07,760 --> 00:03:15,840
5534 modulo 6 and we're going to get a

69
00:03:11,959 --> 00:03:18,519
remainder of -2 so this negative value

70
00:03:15,840 --> 00:03:22,239
divided by 6 gave us that negative all

71
00:03:18,519 --> 00:03:25,760
FS 1 CD but the remainder of that is -2

72
00:03:22,239 --> 00:03:27,599
so all FS 1 CD * 6 would give

73
00:03:25,760 --> 00:03:30,239
you

74
00:03:27,599 --> 00:03:35,560
5532 so remainder of

75
00:03:30,239 --> 00:03:38,360
-2 all right and five modulus 6 and a

76
00:03:35,560 --> 00:03:40,400
remainder of five now remainder with

77
00:03:38,360 --> 00:03:43,720
word size it's exactly what you would

78
00:03:40,400 --> 00:03:46,319
expect take the lower 32 bits of rs1

79
00:03:43,720 --> 00:03:49,040
treat it as signed and do the modulus

80
00:03:46,319 --> 00:03:52,040
operation on the lower 32 bits of rs2

81
00:03:49,040 --> 00:03:54,840
and sign and so again because remainder

82
00:03:52,040 --> 00:03:57,640
is defined in terms of division that's

83
00:03:54,840 --> 00:03:59,120
why the signedness matters here because

84
00:03:57,640 --> 00:04:00,200
the net result of the division you're

85
00:03:59,120 --> 00:04:03,239
going to take the the remainder of

86
00:04:00,200 --> 00:04:06,280
whatever is left over from that so going

87
00:04:03,239 --> 00:04:08,799
back to this example all FS AAC word

88
00:04:06,280 --> 00:04:10,280
size so we grab the bottom 32 bits but

89
00:04:08,799 --> 00:04:12,519
it doesn't matter because it's the same

90
00:04:10,280 --> 00:04:16,560
value in 32bit as it is in

91
00:04:12,519 --> 00:04:20,600
64 modulus 6 and again it is

92
00:04:16,560 --> 00:04:22,800
-2 five modulus 6 is five as always I'm

93
00:04:20,600 --> 00:04:24,240
being very uncreative because I just

94
00:04:22,800 --> 00:04:25,520
wanted to get through these variants I

95
00:04:24,240 --> 00:04:27,400
could have just went with one thing and

96
00:04:25,520 --> 00:04:30,440
not showed you the second one but I gave

97
00:04:27,400 --> 00:04:34,000
you that bonus completely unnecessary no

98
00:04:30,440 --> 00:04:36,560
op of a remainder operation whatever

99
00:04:34,000 --> 00:04:38,680
anyways look at the code step through

100
00:04:36,560 --> 00:04:40,800
the assembly and understand that you're

101
00:04:38,680 --> 00:04:42,800
just dealing with a couple new variants

102
00:04:40,800 --> 00:04:45,440
of the division and remainder that you

103
00:04:42,800 --> 00:04:48,400
already know and love variants for

104
00:04:45,440 --> 00:04:51,199
signed interpretation of variables and

105
00:04:48,400 --> 00:04:54,199
variance for word-sized signed

106
00:04:51,199 --> 00:04:54,199
interpretations

